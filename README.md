Rancher Proxy
=============
Using Rancher (2.x) as a CD platform from GitLab, I have encountered the problem that a token that has been designated for a single cluster cannot be used by their CLI tool. This is a problem when updating one of the application configurations deployed by Rancher, since the project would have to contain a token with full access to the Rancher server, leading to security problems.
This program tries to solve the situation, positioning itself as a proxy, where the proxy is the one with this global access token and only known by the main administrators. An API is exposed that allows to update only the configurations of those deployed applications, protecting the requests with a new token that allows a greater granularity of access.

## Run

To deploy you need set the `APP_SETTINGS` environment variable:

- `config.development` for Develop
- `config.production` for Production

and the variables for Rancher connection:

- `RANCHER_ACCESS_KEY` Token (not scoped) access key
- `RANCHER_SECRET_KEY` Token (not scoped) secret key
- `RANCHER_URL` Rancher `/v3` endpoint

### Production

For production you need set some environment variables:

Variable | Description
-------- | -----------
SECRET_KEY | Secret key for hashes and other crypto
DATABASE_URL | Database URL for SQLAlchemist
INITIAL_ADMIN_USER | Initial administrator user
INITIAL_ADMIN_PASSWORD | Initial administrator password

## Docker

The Docker image is intended for production use, however you can make your own based on the repository image.

```sh
docker build -t rancher_proxy .
docker run <-e ENV_VAR:VALUE> -p 8080:80 -d rancher_proxy
```

## Token administration

You need to access to `/admin` endpoint and log in with the admin account, then there you can manage the tokens accounts and its properties. On `Apps` are the apps allowed to access, separed by ` ` (white space).

## API

### [POST] `/upgrade_app`

Parameter (JSON) | Description
----------------- | -----------
`token` | Token for access
`project_id` | Project ID
`app_name` | Application Name
`app_namespace` | Application Namespace
`data_to_change` | Data to change as `JSON Object`