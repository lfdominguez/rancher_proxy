import os
import traceback
import functions
import helm3

from app import app
from flask import Flask, request, Response


@app.route('/upgrade_app', methods=['POST'])
def upgrade_app():
  if request.is_json:
    content = request.get_json()

    token_str = str(content["token"])

    if token_str == "":
      return Response("{'error':'Token missing'}", status=403, mimetype='application/json')

    try:
      if os.environ['TOKEN_SECRET'] == token_str:
        app.logger.info("Accepted deploy for: {}: {}".format(
            str(content["app_name"]), content["data_to_change"]))

        if "type" in content and content["type"] == "helm":
          helm3.deploy_app(
            k8s_server=os.environ['K8S_SERVER_URL'],
            k8s_user_token=os.environ['K8S_USER_TOKEN'],
            content=content
          )
        else:
          functions.upgrade_app(
              access_key=os.environ['RANCHER_ACCESS_KEY'],
              secret_key=os.environ['RANCHER_SECRET_KEY'],
              rancher_url=os.environ['RANCHER_URL'],
              data_to_change_raw=content["data_to_change"],
              app_name=str(content["app_name"]),
              app_namespace=str(content["app_namespace"]),
              project_id=str(content["project_id"])
          )

        return Response(status=204)
      else:
        return Response("{'error':'Wrong token'}", status=403, mimetype='application/json')

    except Exception as e:
      app.logger.error("Error in Rancher API: {}".format(e))
      return Response("{'error':'Error on Rancher API'}", status=500, mimetype='application/json')
  else:
    return Response("{'error':'The request must contain a JSON'}", status=422, mimetype='application/json')


if __name__ == '__main__':
    app.run()
