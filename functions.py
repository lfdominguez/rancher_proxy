import rancher
import requests
import json
import yaml

from app import app


def upgrade_app(access_key, secret_key, rancher_url, data_to_change_raw, app_name, app_namespace, project_id):
  client = rancher.Client(
      url=rancher_url, access_key=access_key, secret_key=secret_key)
  project = client.by_id_project(project_id)

  for app in project.apps():
    if app["name"] == app_name and app['targetNamespace'] == app_namespace:
      data_to_send = dict()

      answersMode = True

      queue_iter = [[data_to_change_raw, []]]
      data_to_change = {}

      while len(queue_iter) > 0:
        current_element = queue_iter.pop()

        for key, val in current_element[0].items():
          keys_mapped = current_element[1].copy()
          keys_mapped.append(key)

          if isinstance(val, dict):
            queue_iter.insert(0, [val, keys_mapped])
          else:
            data_to_change[".".join(keys_mapped)] = val

      try:
        # Find for answers
        data_to_send["answers"] = app["answers"].data_dict()
        data_to_send["answers"].update(data_to_change)
      except KeyError:
        #Change to valuesYAML
        answersMode = False
        data_to_send["valuesYaml"] = app["valuesYaml"]

        values_yaml = yaml.safe_load(data_to_send["valuesYaml"])

        for key, val in data_to_change.items():
          to_find = key.split(".")

          d = values_yaml
          for key in to_find[:-1]:
            if key in d:
              d = d[key]
            else:
              d = d.setdefault(key, {})

          d[to_find[-1]] = val
        
        queue_iter = list()
        extra_large = dict()

        queue_iter.insert(0, values_yaml)

        while len(queue_iter) > 0:
          current_element = queue_iter.pop()

          for key, val in current_element.items():
            if isinstance(val, dict):
              queue_iter.insert(0, val)
            elif "\n" in str(val):
              extra_large[key] = val
              current_element[key] = ""

        yaml_dump = yaml.dump(values_yaml)

        final_yaml = "---\n"
        for line in iter(yaml_dump.splitlines()):
          key_raw = line.split(":")[0]

          if key_raw.strip() in extra_large.keys():
            final_yaml += "  {}: |\n".format(key_raw)
            for extended_line in iter(extra_large[key_raw.strip()].splitlines()):
              final_yaml += "      {}\n".format(extended_line)
          else:
            final_yaml += "  {}\n".format(line)

        data_to_send["valuesYaml"] = final_yaml

      data_to_send["externalId"] = app["externalId"]

      requests.post(app["actions"]["upgrade"],
                    data=json.dumps(data_to_send),
                    headers={
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer {}:{}".format(access_key, secret_key)
      },
          cookies={},
      )
      
      break
