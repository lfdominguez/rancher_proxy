import subprocess
import yaml

from app import app

def create_kubeconfig_file(k8s_server, k8s_user_token):
    with open("kubeconfig", "w") as kubeconfig_file:
        kubeconfig_file.write("""
apiVersion: v1
clusters:
- cluster:
    server: %s
  name: k8s
contexts:
- context:
    cluster: k8s
    user: k8s-user
  name: k8s
current-context: k8s
kind: Config
preferences: {}
users:
- name: k8s-user
  user:
    token: %s
        """ % (k8s_server, k8s_user_token))

def create_values_file(data_to_change_raw):
    with open("values.yml", "w") as values_yaml:
        values_yaml.write(yaml.dump(data_to_change_raw, default_flow_style=False))

def deploy_app(k8s_server, k8s_user_token, content):

    data_to_change_raw=content["data_to_change"]
    app_name=str(content["app_name"])
    app_namespace=str(content["app_namespace"])

    app_repo_name=str(content["app_repo_name"])
    app_repo_chart=str(content["app_repo_chart"])
    app_repo_url=str(content["app_repo_url"])

    command_add_repo = ["helm", "repo", "add", app_repo_name, app_repo_url]

    command_upgrade = [
      "helm",
      "upgrade",
      "--install",
      "--kubeconfig",
      "kubeconfig",
      "--values",
      "values.yml",
      "--namespace",
      app_namespace,
      app_name,
      "%s/%s" % (app_repo_name, app_repo_chart)
    ]

    create_kubeconfig_file(k8s_server, k8s_user_token)
    create_values_file(data_to_change_raw)

    app.logger.info("Executing helm command: {}".format(command_add_repo))
    app.logger.info("Executing helm command: {}".format(command_upgrade))

    subprocess.call(command_add_repo, shell = False)
    subprocess.call(command_upgrade, shell = False)